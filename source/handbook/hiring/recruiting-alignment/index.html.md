---
layout: markdown_page
title: "Recruiting Alignment"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Recruiter and Coordinator Alignment by Hiring Manager

| Sales                    | Recruiter       | Coordinator                                |
|--------------------------|-----------------|--------------------------------------------|
| Richard Pidgeon          | Nadia Vatalidis | Kike Adio                                  |
| Chad Malchow             | Kelly Murdock   | Kike Adio                                  |
| Michael Alessio          | Nadia Vatalidis | Kike Adio |
| Francis Aquino           | Kelly Murdock   | Kike Adio |
| Paul Almeida             | Kelly Murdock   | Kike Adio |
| Kristen Lawrence         | Kelly Murdock   | Kike Adio |
| Leslie Blanchard         | Kelly Murdock   | Kike Adio |

| Marketing                                      | Recruiter       | Coordinator                                |
|------------------------------------------------|-----------------|--------------------------------------------|
| Erica Lindberg                                 | Jacie Zoerb     | Kike Adio |
| Ashish Kuthiala                                | Jacie Zoerb     | Kike Adio |
| LJ Banks                                       | Jacie Zoerb     | Kike Adio |
| Alex Turner                                    | Jacie Zoerb     | Kike Adio |
| Elsje Smart                                    | Nadia Vatalidis | Kike Adio |
| Director of Field Marketing (to be hired, Leslie Blanchard as interim) | Jacie Zoerb     | Kike Adio |
| Director of Marketing Operations (to be hired, LJ Banks as interim) | Jacie Zoerb     | Kike Adio |
| David Planella                                 | Jacie Zoerb     | Kike Adio |

| Engineering           | Recruiter                                               | Coordinator |
|-----------------------|---------------------------------------------------------|-------------|
| Frontend              | Eva Petreska                                            | Emily Mowry |
| UX                    | Eva Petreska                                            | Emily Mowry |
| Security              | Steve Pestorich                                         | Emily Mowry |
| Backend - Dev         | Trust Ogor                                              | Emily Mowry |
| Quality               | Eva Petreska                                            | Emily Mowry |
| Backend - Ops         | Trust Ogor                                              | Emily Mowry |
| Support               | Nadia Vatalidis                                         | Emily Mowry |
| Infrastructure        | Steve Pestorich                                         | Emily Mowry |

| Product           | Recruiter                       | Coordinator |
|-------------------|---------------------------------|-------------|
| Mark Pundsack     | Nadia Vatalidis                 | Emily Mowry |
| Job van der Voort | Nadia Vatalidis                 | Emily Mowry |

| Other         | Recruiter                       | Coordinator                                |
|---------------|---------------------------------|--------------------------------------------|
| Paul Machle   | Jacie Zoerb/Nadia Vatalidis     | Kike Adio |
| Barbie Brewer | Jacie Zoerb/Nadia Vatalidis     | Kike Adio |
| Brandon Jung  | Kelly Murdock                   | Kike Adio |
| Meltano       | Steve Pestorich                 | Emily Mowry |

## Sourcer Alignment by Division and Location

| Product               | Region            | Sourcer                | Estimated % |
|-----------------------|-------------------|------------------------|-------------|
| Sales & Marketing     | Americas/Anywhere | Stephanie Garza        | 100%        |
| Sales & Marketing     | EMEA & APAC       | Anastasia Pshegodskaya | 25%         |
| Engineering Backend   | Anywhere          | Zsuzsanna Kovacs       | 100%        |
| Engineering General   | Anywhere          | Anastasia Pshegodskaya | 50%         |
| Other/Director+ Roles | Anywhere          | Anastasia Pshegodskaya | 25%         |
