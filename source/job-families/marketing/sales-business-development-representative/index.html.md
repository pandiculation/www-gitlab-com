---
layout: job_family_page
title: "Sales Business Development Representative"
---

You love talking about GitLab to people and no question or challenge is too big or small. You have experience working directly with prospects in order to answer questions on getting started with a technical product. Your job is to make sure our prospects are successful, from the single-shop development firm all the way up to our Fortune 500 customers, and that everyone gets the appropriate level of support.

## Responsibilities

* Meet or exceed Sales Accepted Opportunity (SAO) volume targets
* Work with strategic account leaders to identify and prioritize key accounts to develop
* Working with the sales team to identify net-new opportunities and nurture existing opportunities within target accounts
* Work in collaboration with field and corporate marketing to drive attendance at marketing events
* Attend field marketing events to engage with participants identify opportunities, and to schedule meetings
* Identify and create new qualified opportunities from inbound [MQLs](https://about.gitlab.com/handbook/business-ops/#mql-definition)
* Be able to identify where a prospect is in the sales and marketing funnel and nurture appropriately 
* Work in collaboration with Online Marketing to develop targeted marketing tactics against your assigned target accounts.
* Speed to lead; maintain [1 day SLA](https://about.gitlab.com/handbook/business-ops/#contact-requests) for all contact requests
* Discretion to qualify and disqualify leads when appropriate
* Develop and execute nurture sequences
* Work to introduce more Core users to our subscription offerings, influence usage adoption
* Work closely with sales to identify new pain points to gain a better understanding of the benefits of Ultimate
* Participate in documenting all processes in the handbook and update as needed with our business development manager
* Be accountable for your data and prospecting activities, log everything, take notes early and often


## Requirements

* Experience in sales or client success in a software/tech company is preferred
* Excellent spoken and written English
* Experience in sales, marketing, or customer service
* Experience with CRM and/or marketing automation software
* An understanding of B2B software, Open Source software, and the developer product space is preferred
* Affinity for software and knowledge of the software development process
* Ability to carefully listen
* You are obsessed with making customers happy. You know that the slightest trouble in getting started with a product can ruin customer happiness.
* Passionate about technology and learning more about GitLab
* Be ready to learn how to use GitLab and Git
* Start part-time or full-time depending on situation
* You share our [values](/handbook/values), and work in accordance with those values
* If in EMEA, fluency in spoken and written German and/or other European languages is preferred
* If in APAC, fluency in Mandarin or literacy in Simplified Chinese is required
* If in LATAM, fluency in Portuguese and Spanish is required

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/company/team).

* Qualified candidates receive a short questionnaire from one of our Global Recruiters
* Selected candidates will be invited to schedule a [screening call](/handbook/hiring/#screening-call) with our Global Recruiters
* Next, candidates will be invited to schedule a first interview with a Sales Development Manager
* Candidates will then be invited to schedule an interview with the Director of Sales Development
* Candidates will be invited to schedule a third interview with our CMO
* Finally, candidates may be asked to interview with our CEO
* Successful candidates will subsequently be made an offer via email

Additional details about our process can be found on our [hiring page](/handbook/hiring)
